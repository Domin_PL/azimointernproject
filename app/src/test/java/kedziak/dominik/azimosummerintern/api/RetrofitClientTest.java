package kedziak.dominik.azimosummerintern.api;

import org.junit.Before;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Maybe;
import io.reactivex.MaybeOnSubscribe;
import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import kedziak.dominik.azimosummerintern.data.model.GitRepos;
import kedziak.dominik.azimosummerintern.data.model.GitUser;
import kedziak.dominik.azimosummerintern.data.model.UserDetails;

public class RetrofitClientTest {

    @Mock
    public GitHubService service;

    private GitUser gitUser;

    @Before
    public void setUp(){
        //Init Mockito annotations
        MockitoAnnotations.initMocks(this);

    }

    @Inject
    public RetrofitClientTest() {
        //Init Mockito annotations
        MockitoAnnotations.initMocks(this);

    }


    public Maybe<GitUser> getUserWhenUserExists(String username) {
        gitUser = new GitUser(username);
        MaybeOnSubscribe<GitUser> maybe = emitter -> emitter.onSuccess(gitUser);
        Mockito.when(service.getUser(username))
                .thenReturn(Maybe.create(maybe));
        return service.getUser(username);
        }

    public Maybe<GitUser> getUserWhenUserDoesNotExistOrThrowsAnException(String username){
        gitUser = new GitUser(null);
        MaybeOnSubscribe<GitUser> maybe = emitter -> emitter.onSuccess(gitUser);
        Mockito.when(service.getUser(username)).thenReturn(Maybe.create(maybe));
        return service.getUser(username);
    }


    public Single<List<GitRepos>> getAllReposWhenAreAvailable(String username) {
        String someDescription = "Sample description";
        GitRepos repo = new GitRepos(username, someDescription);
        List<GitRepos> repos = new ArrayList<>();
        repos.add(repo); // Make real list of repos

        SingleOnSubscribe<List<GitRepos>> singleOnSubscribe = emitter -> emitter.onSuccess(repos);
        Mockito.when(service.getAllRepos(username)).thenReturn(Single.create(singleOnSubscribe));

        return service.getAllRepos(username);
    }

    public Single<List<GitRepos>> getAllReposWhenUnavailable(String username){
        GitRepos gitRepo = new GitRepos(null, null);
        List<GitRepos> repos = new ArrayList<>();
        repos.add(gitRepo);

        SingleOnSubscribe<List<GitRepos>> single = emitter -> emitter.onSuccess(repos);

        Mockito.when(service.getAllRepos(username)).thenReturn(Single.create(single));

        return service.getAllRepos(username);
    }

    public Single<UserDetails> getRepoDetailsWhenAvailable(String username) {
        String name = "sampleName";
        String description = "someDescription";
        String languages = "C";
        String openIssues =  "1000";
        String watchers = "10";
        String forks = "23";
        UserDetails details = new UserDetails(
                name, description, languages, openIssues, watchers, forks);

        SingleOnSubscribe<UserDetails> single = emitter -> emitter.onSuccess(details);

        Mockito.when(service.getDetailRepo(username, name)).thenReturn(Single.create(single));

        return service.getDetailRepo(username, name);

    }


}
package kedziak.dominik.azimosummerintern.ui.mainActivity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import kedziak.dominik.azimosummerintern.base.BaseViewModelTest;
import kedziak.dominik.azimosummerintern.data.GitHubRepositoryTest;

import static org.junit.Assert.*;

public class MainViewModelTest extends BaseViewModelTest {

    //sample username
    private static final String username = "AzimoLabs";


    private GitHubRepositoryTest mRepository;
    private Disposable disposable;

    @Before
    public void setUp() {
        mRepository = getGitRepository();
    }

    @Test
    public void onSearchButtonClickedWhenUserExists() {
        disposable = mRepository.getUserWhenExists(username)
                .subscribeOn(Schedulers.io())
                .subscribe(
                        user -> {
                            String login = (user.getLogin());
                            System.out.println("Success");
                            assertEquals(username, login); // Must be called in lambda expression, otherwise
                            //  assertion will be executed before first lambda expression
                        },
                        throwable -> System.out.println("error"));
        onCleared();
    }

    @Test
    public void onSearchButtonClickedWhenUserNotExists(){
        disposable = mRepository.getUserWhenDoesNotExists(username)
                .subscribeOn(Schedulers.io())
                .subscribe(
                        nameUser -> assertEquals(username, nameUser.getLogin()),
                        Assert::assertNull);
        onCleared();
    }


    public void onCleared(){
        if (disposable != null && !disposable.isDisposed())
            disposable.dispose();
    }

}
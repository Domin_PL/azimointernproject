package kedziak.dominik.azimosummerintern.ui.userRepositoriesActivity;

import org.junit.Before;
import org.junit.Test;

import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import kedziak.dominik.azimosummerintern.base.BaseViewModelTest;
import kedziak.dominik.azimosummerintern.data.GitHubRepositoryTest;
import kedziak.dominik.azimosummerintern.data.model.GitRepos;

import static org.junit.Assert.*;

public class RepoViewModelTest extends BaseViewModelTest {

    private GitHubRepositoryTest mRepository;
    private static final String username = "AzimoLabs";

    private Disposable disposable;

    @Before
    public void setUp(){
        mRepository = getGitRepository();
    }


    @Test
    public void getAllReposWhenAvailable() {
        String someDescription = "Sample description";
        disposable = mRepository.getAllReposWhenAvailable(username)
                .subscribeOn(Schedulers.io())
                .subscribe(
                        mRepos -> {
                            assertEquals(1, mRepos.size());
                            GitRepos gitRepo = mRepos.get(0);
                            assertEquals(username, gitRepo.getRepoName());
                            assertEquals(someDescription, gitRepo.getRepoDescription());
                        }
                );
        onCleared();
    }

    @Test
    public void getAllReposWhenUnavailable(){
        disposable = mRepository.getAllReposWhenUnavailable(username)
                .subscribeOn(Schedulers.io())
                .subscribe(
                    gitRepos -> {
                        assertEquals(0, gitRepos.size());
                        GitRepos mRepo = gitRepos.get(0);
                        assertNull(mRepo.getRepoName());
                        assertNull(mRepo.getRepoDescription());
                    });
        onCleared();
    }

    @Test
    public void getRepoDetails() {
        String name = "sampleName";
        String description = "someDescription";
        String languages = "C";
        String openIssues =  "1000";
        String watchers = "10";
        String forks = "23";

        disposable = mRepository.getRepoDetails(username)
                .subscribeOn(Schedulers.io())
                .subscribe(
                        userDetails -> {
                            assertEquals(name, userDetails.getRepoName());
                            assertEquals(description, userDetails.getRepoDescription());
                            assertEquals(languages, userDetails.getRepoLanguage());
                            assertEquals(openIssues, userDetails.getRepoOpenIssues());
                            assertEquals(watchers, userDetails.getRepoWatchers());
                            assertEquals(forks, userDetails.getRepoForks());
                        }
                );
        onCleared();
    }

    public void onCleared(){
        if (disposable != null && !disposable.isDisposed())
            disposable.dispose();
    }

}
package kedziak.dominik.azimosummerintern.base;

import androidx.lifecycle.ViewModel;

import kedziak.dominik.azimosummerintern.data.GitHubRepositoryTest;
import kedziak.dominik.azimosummerintern.di.Components.DaggerRepositoryComponentTest;
import kedziak.dominik.azimosummerintern.di.Components.RepositoryComponentTest;

public abstract class BaseViewModelTest extends ViewModel {

    private GitHubRepositoryTest mGitRepository;

    public BaseViewModelTest() {
        RepositoryComponentTest component
                = DaggerRepositoryComponentTest.create();
        mGitRepository = component.getRepo();
    }

    public GitHubRepositoryTest getGitRepository() {
        return mGitRepository;
    }

}
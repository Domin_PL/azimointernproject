package kedziak.dominik.azimosummerintern.data;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Maybe;
import io.reactivex.Single;
import kedziak.dominik.azimosummerintern.api.RetrofitClientTest;
import kedziak.dominik.azimosummerintern.data.model.GitRepos;
import kedziak.dominik.azimosummerintern.data.model.GitUser;
import kedziak.dominik.azimosummerintern.data.model.UserDetails;

public class GitHubRepositoryTest {

    private RetrofitClientTest mClient;

    @Inject
    public GitHubRepositoryTest(RetrofitClientTest mClient) {
        this.mClient = mClient;
    }


    public Maybe<GitUser> getUserWhenExists (String username) {
        return mClient.getUserWhenUserExists(username);
    }

    public Maybe<GitUser> getUserWhenDoesNotExists(String username){
        return mClient.getUserWhenUserDoesNotExistOrThrowsAnException(username);
    }

    public Single<List<GitRepos>> getAllReposWhenAvailable(String username) {
        return mClient.getAllReposWhenAreAvailable(username);
    }

    public Single<List<GitRepos>> getAllReposWhenUnavailable(String username){
        return mClient.getAllReposWhenUnavailable(username);
    }

    public Single<UserDetails> getRepoDetails(String username) {
        return mClient.getRepoDetailsWhenAvailable(username);
    }
}
package kedziak.dominik.azimosummerintern.di.Components;

import dagger.Component;
import kedziak.dominik.azimosummerintern.api.RetrofitClientTest;
import kedziak.dominik.azimosummerintern.data.GitHubRepositoryTest;

@Component
public interface RepositoryComponentTest {

    GitHubRepositoryTest getRepo();
    RetrofitClientTest getClient();

}
package kedziak.dominik.azimosummerintern.api;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Single;
import kedziak.dominik.azimosummerintern.data.model.GitRepos;
import kedziak.dominik.azimosummerintern.data.model.GitUser;
import kedziak.dominik.azimosummerintern.data.model.UserDetails;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Url;

/**
 * This interface contains available interfaces to make a call with, each returns observable single
 */
public interface GitHubService {

    /**
     * This interface may get user object from GitHub Api.
     * It uses maybe, as it may return value or not (e.g user doesn't exist)
     * @param user name of user to find repos
     * @return Maybe with(out) GitUser object
     */
    @GET("users/{user}")
    Maybe<GitUser> getUser(@Path("user") String user);


    /**
     * This interface is used to make a call with use of Retrofit
     * and takes first 100 repositories which belong to user. If you want
     * to get more you have to modify this call or create a new one.
     * (GitHub API allows only to obtain max 100 repos per page, default value is 30 without this change)
     * @param user nickname of user
     * @return Single with GitRepos object containing list of repos
     */
    @GET("/users/{user}/repos?per_page=100")
    Single<List<GitRepos>> getAllRepos(@Path("user") String user);


    /**
     * This interface is used to make a call to get following details of repo:
     * repository name
     * repo description
     * language
     * open issues
     * forks
     * @param username nickname of user
     * @param repoName name of repository to retrieve details
     * @return Single object with details object
     */
    @GET("/repos/{username}/{repoName}")
    Single<UserDetails> getDetailRepo(@Path("username") String username,
                                          @Path("repoName") String repoName);



}

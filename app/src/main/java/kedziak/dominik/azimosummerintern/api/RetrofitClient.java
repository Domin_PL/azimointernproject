package kedziak.dominik.azimosummerintern.api;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Maybe;
import io.reactivex.Single;
import kedziak.dominik.azimosummerintern.data.model.GitRepos;
import kedziak.dominik.azimosummerintern.data.model.GitUser;
import kedziak.dominik.azimosummerintern.data.model.UserDetails;

/**
 * This is a client for Retrofit, here all calls are being done. No Android related stuff should be here
 */
public class RetrofitClient {

    private GitHubService service;


    //Inject retrofit interface to make calls
    @Inject
    public RetrofitClient(GitHubService service) {
        this.service = service;
    }


    public Maybe<GitUser> getUser(String userName) {
        return service.getUser(userName);
    }


    public Single<List<GitRepos>> getAllRepos(String username){
        return service.getAllRepos(username);

    }

    public Single<UserDetails> getRepoDetails(String username, String repoName){
        return service.getDetailRepo(username, repoName);
    }


}

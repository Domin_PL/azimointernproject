package kedziak.dominik.azimosummerintern.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import kedziak.dominik.azimosummerintern.data.model.GitRepos;
import kedziak.dominik.azimosummerintern.data.model.UserDetails;
import kedziak.dominik.azimosummerintern.interactors.ViewInteractor;

public abstract class BaseActivity extends AppCompatActivity
                implements ViewInteractor {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //TODO: instantiate BaseViewModel and set interface setter here.
    }

    @Override
    public void startNewActivity(String username) {

    }

    @Override
    public void showToast(String message) {

    }

    @Override
    public void setAdapterData(List<GitRepos> repos) {

    }

    @Override
    public void setDetailsData(UserDetails user) {

    }
}

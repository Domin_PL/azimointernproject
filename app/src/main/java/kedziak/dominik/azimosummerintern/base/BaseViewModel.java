package kedziak.dominik.azimosummerintern.base;

import androidx.lifecycle.ViewModel;

import kedziak.dominik.azimosummerintern.data.GitHubRepository;
import kedziak.dominik.azimosummerintern.data.model.UserModel;
import kedziak.dominik.azimosummerintern.di.Components.DaggerRepositoryComponent;
import kedziak.dominik.azimosummerintern.di.Components.RepositoryComponent;
import kedziak.dominik.azimosummerintern.ui.databinding.UiBinder;
import kedziak.dominik.azimosummerintern.ui.userRepositoriesActivity.UserActivityBinder;

/**
 * Base for ViewModel if you want to make network calls. You can extend this one
 * and implement methods only those are needed.
 */
public abstract class BaseViewModel extends ViewModel  {

    private GitHubRepository mRepository;
    private UserModel user;
    private UiBinder uiBinder;
    private UserActivityBinder activityBinder;

    /**
     * Build DaggerComponent and feed Repository constructor.
     * This component also feeds RetrofitClient's constructor.
     */
    public BaseViewModel() {
        RepositoryComponent component
                = DaggerRepositoryComponent.create();
        this.mRepository = component.getRepo();
        this.user = component.getUser();
        this.uiBinder = component.getUI();
        this.activityBinder = component.getUserBinder();
    }

    /**
     * Getters for Repository and User objects, used in ViewModel extending this base model
     */
    protected GitHubRepository getRepository() {
        return mRepository;
    }

    public UserModel getUser() {
        return user;
    }

    public UiBinder getUiBinder() {
        return uiBinder;
    }

    public UserActivityBinder getActivityBinder() {
        return activityBinder;
    }

}

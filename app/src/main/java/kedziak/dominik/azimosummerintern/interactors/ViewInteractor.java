package kedziak.dominik.azimosummerintern.interactors;

import androidx.annotation.Nullable;

import java.util.List;

import kedziak.dominik.azimosummerintern.data.model.GitRepos;
import kedziak.dominik.azimosummerintern.data.model.UserDetails;

public interface ViewInteractor {

    void startNewActivity(String username);

    void showToast(@Nullable String message);

    void setAdapterData(List<GitRepos> repos);

    void setDetailsData(UserDetails user);
}

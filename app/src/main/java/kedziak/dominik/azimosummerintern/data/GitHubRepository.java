package kedziak.dominik.azimosummerintern.data;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Maybe;
import io.reactivex.Single;
import kedziak.dominik.azimosummerintern.api.RetrofitClient;
import kedziak.dominik.azimosummerintern.data.model.GitRepos;
import kedziak.dominik.azimosummerintern.data.model.GitUser;
import kedziak.dominik.azimosummerintern.data.model.UserDetails;

/**
 * This method manages all operations related to database (if maintaining), network calls etc
 * No Android related stuff here.
 */
public class GitHubRepository {


    private RetrofitClient mClient;

    /**
     * Inject constructor, init Retrofit Client
     */
    @Inject
    GitHubRepository(RetrofitClient mClient) {
        this.mClient = mClient;
    }

    /**
     * Call this method to get Maybe with(out) GitUser object, that may contain user's login if their
     * exists in GitHib
     * @param username name of user to check if exists
     * @return Maybe with GitUser object
     */
    public Maybe<GitUser> checkIfUserExists(String username){
        return mClient.getUser(username);
    }

    /**
     * Call this method to get List of repos which belong to user.
     * @param username name of user to retrieve repos
     * @return Single with List of GitRepos object
     */
    public Single<List<GitRepos>> getAllRepos(String username) {
        return mClient.getAllRepos(username);
    }

    /**
     * Call this method to get details about given repository
     * @param name name of user
     * @param repoName name of repo to retrieve their's details
     * @return Single with UserDetails containing details about given repo
     */
    public Single<UserDetails> getRepoDetails(String name, String repoName){
        return mClient.getRepoDetails(name, repoName);
    }

}


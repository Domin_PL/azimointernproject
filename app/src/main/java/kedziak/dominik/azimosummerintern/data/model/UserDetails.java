package kedziak.dominik.azimosummerintern.data.model;

import com.squareup.moshi.Json;

/**
 * This object is created/initialized when user wants to display details about given repository
 */
public class UserDetails {

        //Names of JSON's object's names
        private static final String name = "name";
        private static final String description = "description";
        private static final String language = "language";
        private static final String openIssues = "open_issues_count";
        private static final String watchers = "watchers_count";
        private static final String forks = "forks_count";

        public UserDetails(String repoName, String repoDescription,
                           String repoLanguage,
                           String repoOpenIssues,
                           String repoWatchers,
                           String repoForks) {
                this.repoName = repoName;
                this.repoDescription = repoDescription;
                this.repoLanguage = repoLanguage;
                this.repoOpenIssues = repoOpenIssues;
                this.repoWatchers = repoWatchers;
                this.repoForks = repoForks;
        }

        @Json(name = name)
        private String repoName;

        @Json(name = description)
        private String repoDescription;

        @Json(name = language)
        private String repoLanguage;

        @Json(name = openIssues)
        private String repoOpenIssues;

        @Json(name = watchers)
        private String repoWatchers;

        @Json(name = forks)
        private String repoForks;

        public String getRepoName() {
                return repoName;
        }

        public String getRepoDescription() {
                return repoDescription;
        }

        public String getRepoLanguage() {
                return repoLanguage;
        }

        public String getRepoOpenIssues() {
                return repoOpenIssues;
        }

        public String getRepoWatchers() {
                return repoWatchers;
        }

        public String getRepoForks() {
                return repoForks;
        }
}

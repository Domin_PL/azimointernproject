package kedziak.dominik.azimosummerintern.data.model;


import android.text.TextUtils;
import android.util.Log;

import javax.inject.Inject;

/**
 * This object is used to assign value from EditText (MainActivity)
 */
public class UserModel  {

    @Inject
    public UserModel() {
    }

    private String mUsername;

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String mUsername) {
        this.mUsername = mUsername;
        Log.d("UserName", mUsername);
    }

    public boolean startsWithDash(){
        return getUsername().startsWith("-");
    }

    /**
     * Check if typed UserName into EditText is empty
     * This method is only called when Search Button is clicked.
     * @return boolean whether it is empty or not
     */
    public boolean isEditTextEmpty() {
        return TextUtils.isEmpty(getUsername());
    }


}

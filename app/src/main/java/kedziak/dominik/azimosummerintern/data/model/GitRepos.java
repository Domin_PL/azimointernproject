package kedziak.dominik.azimosummerintern.data.model;

import com.squareup.moshi.Json;

/**
 * This object is used to assign name and description of repository. To make a list we create list
 * of GitRepos object to display them in RecyclerView.
 */
public class GitRepos {

    private static final String mName = "name";
    private static final String mDescription = "description";

    /**
     * Serialize names to be visible for Moshi
     */
    @Json(name = mName)
    private String repoName;

    @Json(name = mDescription)
    private String repoDescription;


    public GitRepos(String repoName, String repoDescription) {
        this.repoName = repoName;
        this.repoDescription = repoDescription;
    }



    public String getRepoName() {
        return repoName;
    }

    public String getRepoDescription() {
        return repoDescription;
    }

}

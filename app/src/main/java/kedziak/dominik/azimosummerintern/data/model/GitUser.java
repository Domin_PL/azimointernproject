package kedziak.dominik.azimosummerintern.data.model;

import com.squareup.moshi.Json;

/**
 * This object is used when checking if user exists on GitHub. If exists, returns this object
 * with assigned login (response code 200...300), if not, throws throwable
 */
public class GitUser {
    public GitUser(String login) {
        this.login = login;
    }

    @Json(name = "login")
    private String login;

    public String getLogin() {
        return login;
    }
}

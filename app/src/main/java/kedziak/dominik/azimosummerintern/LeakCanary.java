package kedziak.dominik.azimosummerintern;

import android.app.Application;

import static com.squareup.leakcanary.LeakCanary.install;
import static com.squareup.leakcanary.LeakCanary.isInAnalyzerProcess;

public class LeakCanary extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        if (isInAnalyzerProcess(this)) return;
        install(this);
    }
}

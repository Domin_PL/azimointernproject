package kedziak.dominik.azimosummerintern.util;

public class Constants {
    //Constant which is used as a key when sending additional values between activities
    public static final String INTENT_EXTRA_STRING = "extra_string";
    //Constant used as an error definition when EditText contains no typed value
    public static final int KEY_EDITTEXT_EMPTY = 1;
    //Constant used as an error value when EditText starts with dash '-'
    public static final int KEY_EDITTEXT_STARTS_DASH = 2;
    //code 200 means that connection was made successfully and data can be fetched
    public static final int URL_CODE_OK = 200;
    //code 404 means that requested field/data/resource wasn't found on the server
    public static final int URL_CODE_NOT_FOUND = 404;

    public static final int SCREEN_REPOSITORY = 0;
    public static final int SCREEN_DETAILS = 1;
    public static final int SCREEN_FETCHING_REPOS = 2;
    public static final int SCREEN_FETCHING_DETAILS = 3;
}

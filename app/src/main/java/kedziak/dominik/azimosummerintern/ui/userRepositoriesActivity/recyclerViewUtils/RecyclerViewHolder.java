package kedziak.dominik.azimosummerintern.ui.userRepositoriesActivity.recyclerViewUtils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import kedziak.dominik.azimosummerintern.databinding.ItemsRecyclerviewBinding;

class RecyclerViewHolder extends RecyclerView.ViewHolder {

    final ItemsRecyclerviewBinding binding;

    RecyclerViewHolder(@NonNull final ItemsRecyclerviewBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

}

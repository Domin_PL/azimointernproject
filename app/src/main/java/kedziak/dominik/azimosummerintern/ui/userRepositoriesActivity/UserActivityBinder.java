package kedziak.dominik.azimosummerintern.ui.userRepositoriesActivity;

import androidx.databinding.ObservableInt;

import javax.inject.Inject;

import kedziak.dominik.azimosummerintern.ui.databinding.UiBinder;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static kedziak.dominik.azimosummerintern.util.Constants.*;

public class UserActivityBinder extends UiBinder {

    public ObservableInt vEmptyViewVisibility = new ObservableInt();
    public ObservableInt vDetailScreenVisibility = new ObservableInt();
    public ObservableInt vRepoListViewVisibility = new ObservableInt();
    private int CURRENT_SCREEN = SCREEN_FETCHING_REPOS;

    @Inject
    public UserActivityBinder() {

    }



    void setFetchingData(int screen){
        CURRENT_SCREEN = screen;
        setProgressBar(VISIBLE);
        vEmptyViewVisibility.set(GONE);
        vRepoListViewVisibility.set(GONE);
        vDetailScreenVisibility.set(GONE);
    }

    void setOnReposFetched(boolean isAnyRepo){
        CURRENT_SCREEN = SCREEN_REPOSITORY;
        setProgressBar(GONE);
        vDetailScreenVisibility.set(GONE);
        if (isAnyRepo){
            vEmptyViewVisibility.set(GONE);
            vRepoListViewVisibility.set(VISIBLE);
        } else {
            vEmptyViewVisibility.set(VISIBLE);
            vRepoListViewVisibility.set(GONE);
        }
    }

    void setOnDetailsFetched(){
        CURRENT_SCREEN = SCREEN_DETAILS;
        setProgressBar(GONE);
        vEmptyViewVisibility.set(GONE);
        vRepoListViewVisibility.set(GONE);
        vDetailScreenVisibility.set(VISIBLE);
    }

    boolean canGoBack(){
        boolean canGoBack = CURRENT_SCREEN == SCREEN_FETCHING_REPOS
                || CURRENT_SCREEN == SCREEN_REPOSITORY;
        if (canGoBack) return true;
        else {
            //Implement custom "back" button behaviour
            // Current screen is or fetching repos, or onDetails screen, close them and show repo list
            setOnReposFetched(true); // If fetching details/ details screen was visible, there is no way,
            // that there were no repos, so set setOnReposFetched() to true, and return false,
            // as can't go back
            return false;
        }
    }


    @Override
    public ObservableInt getProgressBarVisibility() {
        return super.getProgressBarVisibility();
    }

    @Override
    public void setProgressBar(int visibility) {
        super.setProgressBar(visibility);
    }

}

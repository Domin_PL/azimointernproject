package kedziak.dominik.azimosummerintern.ui.userRepositoriesActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.List;
import java.util.Objects;

import kedziak.dominik.azimosummerintern.R;
import kedziak.dominik.azimosummerintern.base.BaseActivity;
import kedziak.dominik.azimosummerintern.data.model.GitRepos;
import kedziak.dominik.azimosummerintern.data.model.UserDetails;
import kedziak.dominik.azimosummerintern.databinding.ActivityRepositoriesBinding;
import kedziak.dominik.azimosummerintern.ui.userRepositoriesActivity.recyclerViewUtils.ReposRVAdapter;
import kedziak.dominik.azimosummerintern.ui.userRepositoriesActivity.recyclerViewUtils.rvClickListener;

import static android.view.View.VISIBLE;
import static kedziak.dominik.azimosummerintern.util.Constants.INTENT_EXTRA_STRING;

public class UserActivity extends BaseActivity
                implements rvClickListener {

    private RepoViewModel mViewModel;
    private ActivityRepositoriesBinding binding;
    private ReposRVAdapter mAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_repositories);

        String name = getIntent().getStringExtra(INTENT_EXTRA_STRING);
        if (name == null) name = ""; // if for any reason it was null, Log.d would throw NullPointerException
        Log.d("Received string: ", name);

        setTitle(name);

        mViewModel = ViewModelProviders.of(this)
                        .get(RepoViewModel.class);

        binding.setViewModel(mViewModel);

        mViewModel.setUserModel(name);
        init();

    }

    /**
     * Init views
     */
    private void init(){
        mViewModel.setViewInteractor(this);
        mViewModel.getAllRepos();

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        binding.recyclerview.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerview.setHasFixedSize(true);
        mAdapter = new ReposRVAdapter();
        binding.recyclerview.setAdapter(mAdapter);
        mAdapter.setOnClickListener(this);
    }


    /**
     * Callback to onClickListener from RecyclerView
     * @param repo name of repo to retrieve details about
     */
    @Override
    public void onItemClicked(String repo) {
        binding.recyclerview.setVisibility(View.GONE);
        binding.progressBar.setVisibility(VISIBLE);
        mViewModel.getRepoDetails(repo);

    }

    /**
     * Override normal back button behaviour and assign new behaviour to up button.
     * If details fragment is displayed just close it, when isn't go back.
     */
    @Override
    public void onBackPressed() {
        if (mViewModel.canGoBack())
            super.onBackPressed();
    }

    @Override
    public void setAdapterData(List<GitRepos> repos) {
        mAdapter.setData(repos);
        mViewModel.clearDisposable();
    }

    /**
     * Callback from ViewModel
     * Sets text on the screen with proper details
     * @param details contains repository details to display on the screen
     */
    @SuppressLint("SetTextI18n")
    @Override
    public void setDetailsData(UserDetails details) {
        String newline = "\n";
        String tab = "\t";
        binding.progressBar.setVisibility(View.GONE);
        binding.repoTextviews.linearlayout.setVisibility(VISIBLE);
        binding.repoTextviews.repoNameTextview.setText
                (getString(R.string.repo_name) + newline + details.getRepoName());
        binding.repoTextviews.repoDescTextview.setText
                (getString(R.string.repo_description) + newline + details.getRepoDescription());
        binding.repoTextviews.languageTextview.setText
                (getString(R.string.language) + tab + details.getRepoLanguage());
        binding.repoTextviews.openissuesTextview.setText
                (getString(R.string.openissues)  + tab + details.getRepoOpenIssues());
        binding.repoTextviews.watchersTextview.setText
                (getString(R.string.watchers)  + tab + details.getRepoWatchers());
        binding.repoTextviews.forksTextview.setText
                (getString(R.string.forks_no)  + tab + details.getRepoForks());
        mViewModel.clearDisposable();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } return super.onOptionsItemSelected(item);
    }

    @Override
    public void showToast(String message) {
        super.showToast(message);
        if (message == null)
            Toast.makeText(UserActivity.this.getApplicationContext(),
                    getString(R.string.error_downloading), Toast.LENGTH_SHORT).show();
    }
}

package kedziak.dominik.azimosummerintern.ui.userRepositoriesActivity;

import android.util.Log;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import kedziak.dominik.azimosummerintern.base.BaseViewModel;
import kedziak.dominik.azimosummerintern.data.GitHubRepository;
import kedziak.dominik.azimosummerintern.data.model.UserModel;
import kedziak.dominik.azimosummerintern.interactors.ViewInteractor;

import static kedziak.dominik.azimosummerintern.util.Constants.*;

public class RepoViewModel extends BaseViewModel {

    private GitHubRepository mRepository;
    private UserModel user;
    public UserActivityBinder mBinder;
    private volatile Disposable disposable;
    private ViewInteractor mViewInteractor;

    void setViewInteractor(ViewInteractor viewInteractor){
        this.mViewInteractor = viewInteractor;
    }

    public RepoViewModel() {
        this.mRepository = getRepository();
        this.user = getUser();
        this.mBinder = getActivityBinder();
    }

    void getAllRepos(){
        mBinder.setFetchingData(SCREEN_FETCHING_REPOS);
        String username = user.getUsername().trim();
        disposable = mRepository.getAllRepos(username)
                   .subscribeOn(Schedulers.io())
                   .observeOn(AndroidSchedulers.mainThread())
                   .subscribe(
                           repos -> {
                                int length = repos.size();
                                mBinder.setOnReposFetched(length > 0);
                                if (length > 0) mViewInteractor.setAdapterData(repos);
                           },
                           throwable -> {
                               Log.e("VMonFailure: ", throwable.getMessage(), throwable);
                               mBinder.setOnReposFetched(true); // True, as there has been an error while fetching repos, e.g no network
                               mViewInteractor.showToast(null);
                           }
                   );
    }

    void getRepoDetails(String repoName){
         mBinder.setFetchingData(SCREEN_FETCHING_DETAILS);
         disposable = mRepository.getRepoDetails(user.getUsername(), repoName)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
         .subscribe(
                 user -> {
                     mBinder.setOnDetailsFetched();
                     mViewInteractor.setDetailsData(user);
                 },
                 throwable -> {
                     mBinder.setOnDetailsFetched();
                     mViewInteractor.showToast(null);}
         );

    }

    void setUserModel(String name){
        user.setUsername(name);
    }


    void clearDisposable(){
        if (disposable != null && !disposable.isDisposed())
            disposable.dispose();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        clearDisposable();
        mBinder = null;
    }

    boolean canGoBack(){
        return mBinder.canGoBack();
    }
}

package kedziak.dominik.azimosummerintern.ui.databinding;

import android.view.View;

import androidx.databinding.ObservableInt;

import javax.inject.Inject;

public class UiBinder {

    @Inject
    public UiBinder() {

    }

    public void setDefaults(){
        //set default values
        vProgressBarVisibility.set(View.GONE);
        vNoUserErrorVisibility.set(View.GONE);
    }

    // UI binders, v - shortcut from variable, use only with UI related variables
    public ObservableInt vProgressBarVisibility = new ObservableInt();

    public ObservableInt vNoUserErrorVisibility = new ObservableInt();

    public ObservableInt vErrorCode = new ObservableInt();


    public void setProgressBar(int visibility){
        vProgressBarVisibility.set(visibility);
    }

    public void setErrorTextViewVisibility(int visibility){
        vNoUserErrorVisibility.set(visibility);
    }

    public void setEditTextError(int code){
        vErrorCode.set(code);
    }


    public ObservableInt getProgressBarVisibility() {
        return vProgressBarVisibility;
    }

}

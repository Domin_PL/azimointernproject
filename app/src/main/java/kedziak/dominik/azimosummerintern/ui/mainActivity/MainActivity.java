
package kedziak.dominik.azimosummerintern.ui.mainActivity;

import android.content.Intent;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import kedziak.dominik.azimosummerintern.R;
import kedziak.dominik.azimosummerintern.base.BaseActivity;
import kedziak.dominik.azimosummerintern.databinding.ActivityMainBinding;
import kedziak.dominik.azimosummerintern.ui.userRepositoriesActivity.UserActivity;

import static kedziak.dominik.azimosummerintern.util.Constants.INTENT_EXTRA_STRING;

public class MainActivity extends BaseActivity {

    private MainViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = ViewModelProviders.of(this)
                .get(MainViewModel.class);

        //Inflate dataBinding activity
        ActivityMainBinding binding =
                DataBindingUtil.setContentView(this, R.layout.activity_main);

        // set ViewModel to dataBinding layout
        binding.setViewModel(mViewModel);

        mViewModel.setViewInteractor(this);
    }

    @Override
    public void startNewActivity(String username) {
        Intent intent = new Intent(MainActivity.this, UserActivity.class);
        intent.putExtra(INTENT_EXTRA_STRING, username);
        MainActivity.this.startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mViewModel != null) {
            mViewModel.setViewInteractor(null);
            mViewModel = null;
        }
    }
}


package kedziak.dominik.azimosummerintern.ui.databinding;

import android.widget.EditText;

import androidx.databinding.BindingAdapter;

import kedziak.dominik.azimosummerintern.R;

import static kedziak.dominik.azimosummerintern.util.Constants.KEY_EDITTEXT_EMPTY;
import static kedziak.dominik.azimosummerintern.util.Constants.KEY_EDITTEXT_STARTS_DASH;


public class BindingAdapters {

    @BindingAdapter("textError")
    public static void setErrorIfWrongData(EditText editText, int code){
        if (code == KEY_EDITTEXT_EMPTY) {
            String editTextEmpty  = editText.getResources().getString(R.string.edittext_null);
            editText.setError(editTextEmpty);
        } else if (code == KEY_EDITTEXT_STARTS_DASH) {
            String editTextDash = editText.getResources().getString(R.string.edittext_dash);
            editText.setError(editTextDash);
        }
    }


}

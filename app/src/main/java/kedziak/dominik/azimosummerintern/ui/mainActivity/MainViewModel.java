package kedziak.dominik.azimosummerintern.ui.mainActivity;

import android.util.Log;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import kedziak.dominik.azimosummerintern.base.BaseViewModel;
import kedziak.dominik.azimosummerintern.data.GitHubRepository;
import kedziak.dominik.azimosummerintern.data.model.UserModel;
import kedziak.dominik.azimosummerintern.interactors.ViewInteractor;
import kedziak.dominik.azimosummerintern.ui.databinding.UiBinder;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static kedziak.dominik.azimosummerintern.util.Constants.KEY_EDITTEXT_EMPTY;
import static kedziak.dominik.azimosummerintern.util.Constants.KEY_EDITTEXT_STARTS_DASH;

/**
 * This ViewModel contains all data which is displayed in the View.
 * View is connected to this ViewModel via dataBinding. No custom adapters appear in this class
 */
public class MainViewModel extends BaseViewModel{

    private UserModel user;
    private GitHubRepository mRepository;
    public UiBinder mBinder;  // must be public
    private volatile Disposable disposable;
    private ViewInteractor mViewInteractor;

    public void setViewInteractor(ViewInteractor viewInteractor){
        this.mViewInteractor = viewInteractor;
    }

    /**
     * Inject Repository constructor
     */

    public MainViewModel() {
        this.user = getUser();
        this.mRepository = getRepository();
        this.mBinder = getUiBinder();
        mBinder.setDefaults();
    }


    /**
     * This method is invoked when search button is clicked.
     * It works in 2 cases:
     * 1): When EditText contains no text, shows an error
     * 2): When EditText contains text which starts with dash '-'
     * 3): When username is not empty calls searchUser() method to call with API
     *
     * become familiar with GitHub's nicknames restrictions:
     * GitHub:
     *   Min: 1
     *   Max: 39  <--  Set up by xml file
     *   Can contain: a-z A-Z 0-9 -   <-- Set up by xml file
     *   Other: Cannot start with a dash
     */
    public void onSearchButtonClicked(){
        boolean isEditTextEmpty = user.isEditTextEmpty();
        // @value startsWithDash must be initialized by default
        boolean startsWithDash = false;
        if (!isEditTextEmpty)
            startsWithDash = user.startsWithDash();
        Log.i("isEditTextEmpty: ", String.valueOf(isEditTextEmpty));
        Log.i("Starts with dash: ", String.valueOf(startsWithDash));
        if (isEditTextEmpty)
            showError(KEY_EDITTEXT_EMPTY);
        else if (startsWithDash)
            showError(KEY_EDITTEXT_STARTS_DASH);
        else searchUser();
    }


    private void searchUser() {
        final String username = getUser().getUsername().trim();
        mBinder.setProgressBar(VISIBLE);
        disposable = mRepository.checkIfUserExists(username)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        gitUser -> {
                            mBinder.setErrorTextViewVisibility(GONE);
                            mBinder.setProgressBar(GONE);
                            mViewInteractor.startNewActivity(username);
                            onCleared();
                        },
                        throwable -> {
                            mBinder.setProgressBar(GONE);
                            mBinder.setErrorTextViewVisibility(VISIBLE);
                            Log.e("UserOnFailure", throwable.getMessage(), throwable);
                            onCleared();
                        }
                );
    }

    /**
     * This method uses interface to call View to show an error, that EditText is empty
     * @param error constant containing type of error
     */
    private void showError(int error) {
        mBinder.setEditTextError(error);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposable != null && !disposable.isDisposed())
            disposable.dispose();
    }
}

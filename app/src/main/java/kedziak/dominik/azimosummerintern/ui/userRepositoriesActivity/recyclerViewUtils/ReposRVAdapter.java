package kedziak.dominik.azimosummerintern.ui.userRepositoriesActivity.recyclerViewUtils;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import kedziak.dominik.azimosummerintern.R;
import kedziak.dominik.azimosummerintern.databinding.ItemsRecyclerviewBinding;
import kedziak.dominik.azimosummerintern.data.model.GitRepos;

public class ReposRVAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {

    private List<GitRepos> mList;
    private LayoutInflater mInflater;
    private rvClickListener mClickListener;

    public void setOnClickListener(rvClickListener listener){
        this.mClickListener = listener;
    }


    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (mInflater == null)
            mInflater = LayoutInflater.from(parent.getContext());
        ItemsRecyclerviewBinding binding =
                DataBindingUtil.inflate(
                        mInflater,
                        R.layout.items_recyclerview,
                        parent,
                        false);
        return new RecyclerViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        final GitRepos repo = mList.get(position);

        String repoName = repo.getRepoName();
        holder.binding.repoNameTextview.setText(repo.getRepoName());
        holder.binding.repoDescTextview.setText(repo.getRepoDescription());
        holder.binding.constraintLayout.setOnClickListener(
                v -> mClickListener.onItemClicked(repoName));

    }

    @Override
    public int getItemCount() {
        if (mList != null)
            return mList.size();
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setData(List<GitRepos> list){
        this.mList = list;
        notifyDataSetChanged();
    }
}

package kedziak.dominik.azimosummerintern.di.Components;

import dagger.Component;
import kedziak.dominik.azimosummerintern.data.GitHubRepository;
import kedziak.dominik.azimosummerintern.data.model.UserModel;
import kedziak.dominik.azimosummerintern.di.Modules.RetrofitModule;
import kedziak.dominik.azimosummerintern.api.RetrofitClient;
import kedziak.dominik.azimosummerintern.ui.databinding.UiBinder;
import kedziak.dominik.azimosummerintern.ui.userRepositoriesActivity.UserActivityBinder;

/**
 * Component which feeds GitHubRepository constructor and is enough
 * to feed RetrofitClient class
 */
@Component(modules = RetrofitModule.class)
public interface RepositoryComponent {

    GitHubRepository getRepo();
    UserModel getUser();
    RetrofitClient getRetrofitClient();
    UiBinder getUI();
    UserActivityBinder getUserBinder();

}

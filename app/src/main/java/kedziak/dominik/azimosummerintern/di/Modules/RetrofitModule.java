package kedziak.dominik.azimosummerintern.di.Modules;

import dagger.Module;
import dagger.Provides;
import kedziak.dominik.azimosummerintern.api.GitHubService;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

@Module
public class RetrofitModule {

    // This is a base url which will be used to fetch data
    private static final String BASE_GITHUB_URL = "https://api.github.com/";

    private GitHubService service;

    public RetrofitModule() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_GITHUB_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        this.service = retrofit.create(GitHubService.class);

    }

    @Provides
    GitHubService getGitService(){
        return service;
    }
}

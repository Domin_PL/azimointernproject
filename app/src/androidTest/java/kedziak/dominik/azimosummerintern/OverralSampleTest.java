package kedziak.dominik.azimosummerintern;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.rule.ActivityTestRule;
import kedziak.dominik.azimosummerintern.ui.mainActivity.MainActivity;
import kedziak.dominik.azimosummerintern.ui.userRepositoriesActivity.UserActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.intent.Intents.init;
import static androidx.test.espresso.intent.Intents.intended;
import static androidx.test.espresso.intent.Intents.release;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.matcher.ViewMatchers.hasErrorText;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.hamcrest.core.IsNot.not;


public class OverralSampleTest {

    /**
     * Instead of creating tests in each class I'll keep them all in one file
     */

    //Set up rule for testing
    @Rule
    public ActivityTestRule rule = new ActivityTestRule<>(MainActivity.class);

    @Rule
    public ActivityTestRule userRule = new ActivityTestRule<>(UserActivity.class);

    // get some strings from resources, variables' names should be clear to understand
    private String editTextError;
    private String editTextNull;
    private String userNotFound;

    @Before
    public void setUp() {
        // Context is needed to access Android resources
        Activity activity = rule.getActivity();
        Context context = activity.getApplicationContext();
        editTextError = context.getString(R.string.edittext_dash);
        editTextNull = context.getString(R.string.edittext_null);
        userNotFound = context.getString(R.string.user_not_found);
    }
    //Initialize some variables instead of using long expression "onView(withId)" each time
    //EditText in MainActivity:
    private ViewInteraction editText = onView(withId(R.id.login_edittext));
    //SearchButton
    private ViewInteraction searchButton = onView(withId(R.id.search_button));
    //Universal progressbar which is reusable
    private ViewInteraction progressBar = onView(withId(R.id.progress_bar));
    //recyclerview
    private ViewInteraction recyclerView = onView(withId(R.id.recyclerview));
    //details fragment
    private ViewInteraction detailsFragment = onView(withId(R.id.repo_textviews));


    @Test
    public void checkEditTextErrorsWhenDashOrEmpty() {
        //Write some invalid data to EditText and check if it works
        editText.perform(typeText("-showThatError"));
        searchButton.perform(click());
        editText.check(matches(hasErrorText(editTextError)))
                .perform(clearText()); // if assertion was successful clear typed text
        //hit search again to check if an error occurs
        searchButton.perform(click());
        editText.check(matches(hasErrorText(editTextNull)));

    }

    @Test
    public void checkInvalidNameThrowsError() throws InterruptedException {
        //Write notExisting name and check if progressbar is being shown
        editText.perform(typeText("blalblballballblb"));
        searchButton.perform(click());
        progressBar.check(matches(isDisplayed()));
        //Let thread sleep for the moment when data is being fetched
        Thread.sleep(4000);
        //check if error has been displayed
        onView(withId(R.id.error_textview)).check(matches(isDisplayed()))
                .check(matches(withText(userNotFound)));
    }

    @Test
    public void userExistsAndHasNoRepos() throws InterruptedException {
        //clear edittext and write some existing user username with no repos
        editText.perform(clearText()).perform(typeText("ananasek")); // change this field if user adds any repo
        //check progressbar again
        searchButton.perform(click());
        progressBar.check(matches(isDisplayed()));
        //Wait for response
        Thread.sleep(4000);
        //check if empty view is displayed
        onView(withId(R.id.no_repos_layout)).check(matches(isDisplayed()));
    }

    @Test
    public void userExistsHasReposAndOpensDetails() throws InterruptedException {
        //Instantiate rv tools to click on item
        editText.perform(typeText("AzimoLabs"));
        searchButton.perform(click());
        progressBar.check(matches(isDisplayed()));
        Thread.sleep(4000);
        recyclerView.check(matches(isDisplayed()));
        recyclerView.perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        recyclerView.check(matches(not(isDisplayed())));
        detailsFragment.check(matches(isDisplayed()));
        pressBack(); // go back
    }




    /**
     * as I didn't create a new intent here, so I can't use Espresso intents.intented to check if
     * new activity was opened, or I did something wrong?
     * below is testMethod
     * */
    @Test
    public void checkIfNewActivityOpens() throws InterruptedException {
        onView(withId(R.id.login_edittext)).perform(typeText("AzimoLabs"));
        onView(withId(R.id.search_button)).perform(click());
        onView(withId(R.id.progress_bar)).check(matches(isDisplayed()));
        Thread.sleep(4000);
        init();
        intended(hasComponent(new ComponentName(getInstrumentation().getContext(), UserActivity.class.getName())));
        release();
    }

}
